## Import necessary modules and define Lame parameters
using Gridap
using GridapEmbedded
using GridapGmsh
using LinearAlgebra: tr

function lame_parameters(E, ν)
    λ = (E * ν) / ((1 + ν) * (1 - 2 * ν))
    μ = E / (2 * (1 + ν))
    return (λ, ν)
end

## Define material parameters
# Material parameters 1
E1 = 59.0 
ν1 = 0.45
λ1, μ1 = lame_parameters(E1,ν1)
σ1(ε) = λ1*tr(ε)*one(ε) + 2*μ1*ε

# Material parameters 2
E2 = 59.0 
ν2 = 0.45
λ2, μ2 = lame_parameters(E2,ν2)
σ2(ε) = λ2*tr(ε)*one(ε) + 2*μ2*ε

# Dirichlet values
u0 = VectorValue(0,0,0)

# Load vector
scale=1.0
g =scale*VectorValue(0, 0, -1.0925e-4)

## Load meniscus model 
bgmodel = GmshDiscreteModel("meshes/MEDIALMENISCUS.gmsh2")
Ω_bg = Triangulation(bgmodel)

## Define relevant geometries
# Cut off a piece for imposing no-displacement boundary conditions

# Define Dirichlet cut plane geometry
midpoint_D = Point(120, 50, 0)
plane_normal_D = VectorValue(-1,0,0)
plane_D = plane(x0=midpoint_D, v=plane_normal_D, name="dirichlet")

# Define knife cut plane geometry
cut_midpoint = Point(0, 15.123, 0)
cut_plane_normal = VectorValue(0,1,0)
cut_plane = plane(x0=cut_midpoint, v=cut_plane_normal, name="cut_plane")

# Discretization and integration meshes
geo_front = intersect(plane_D, cut_plane, name="front")
geo_back =  intersect(plane_D, !(cut_plane), name="back")
cutgeo = cut(bgmodel,union(geo_front,geo_back))

## Setup interpolation and integration meshes

# Set up front part
Ω1_act = Triangulation(cutgeo, ACTIVE, "front")
Ω1 = Triangulation(cutgeo, PHYSICAL, "front")

writevtk(Ω1_act, "Omega_front_act")
writevtk(Ω1, "Omega_front")

# Set up back part
Ω2_act = Triangulation(cutgeo, ACTIVE, "back")
Ω2 = Triangulation(cutgeo,PHYSICAL,"back")

writevtk(Ω2_act, "Omega_back_act")
writevtk(Ω2, "Omega_back")

# Setup boundaries and interfaces
Γ_cut = EmbeddedBoundary(cutgeo,"front","back")
Γ_D = EmbeddedBoundary(cutgeo,"dirichlet")

writevtk(Γ_cut, "Gamma_cut")
writevtk(Γ_D, "Gamma_D")

# Set up ghost penalties 
Γg1 = GhostSkeleton(cutgeo, "front")
Γg2 = GhostSkeleton(cutgeo, "back")

writevtk(Γg1, "Ghostskeleton_front")
writevtk(Γg2, "Ghostskeleton_back")

# Setup normal vectors
n_Γ_cut = get_normal_vector(Γ_cut)
n_Γ_D = get_normal_vector(Γ_D)

n_Γg1 = get_normal_vector(Γg1)
n_Γg2 = get_normal_vector(Γg2)

# Setup Lebesgue measures 
order = 1
degree = 2*order
dΩ1 = Measure(Ω1,degree)
dΩ2 = Measure(Ω2,degree)
dΓ_cut = Measure(Γ_cut,degree)
dΓ_D = Measure(Γ_D,degree)
dΓg1 = Measure(Γg1,degree)
dΓg2 = Measure(Γg2,degree)

## Setup FESpace
V1 = TestFESpace(Ω1_act,
                 ReferenceFE(lagrangian,VectorValue{3,Float64},order),
                 conformity=:H1)
V2 = TestFESpace(Ω2_act,
                 ReferenceFE(lagrangian,VectorValue{3,Float64},order),
                 conformity=:H1)

U1 = TrialFESpace(V1)
U2 = TrialFESpace(V2)

V = MultiFieldFESpace([V1,V2])
U = MultiFieldFESpace([U1,U2])

## Set up weak variation formulation

# Setup stabilization parameters
meas_K1 = get_cell_measure(Ω1, Ω_bg)
meas_K2 = get_cell_measure(Ω2, Ω_bg)
meas_KΓ = get_cell_measure(Γ_cut, Ω_bg)

γ_hat = 2
κ1 = CellField( (E2*meas_K1) ./ (E2*meas_K1 .+ E1*meas_K2), Ω_bg)
κ2 = CellField( (E1*meas_K2) ./ (E2*meas_K1 .+ E1*meas_K2), Ω_bg)
β_cut  = CellField( (γ_hat*meas_KΓ) ./ ( meas_K1/E1 .+ meas_K2/E2 ), Ω_bg)
β_D  = 50000.0
β1 = 0.02
β2 = 0.02

meas_only_K1 = get_cell_measure(Ω1_act)
meas_only_K2 = get_cell_measure(Ω2_act)
h1 = CellField(meas_only_K1.^(1/3.0) , Ω1_act)
h2 = CellField(meas_only_K2.^(1/3.0) , Ω2_act)

# Describe where we switch between cut and non-cut interface boundary condition
T = 7
dt = 0.35
blade_length = 1
cut_speed = 1

## Run time loop 
createpvd("LinearElasticityDynamicsFront") do pvd1
  createpvd("LinearElasticityDynamicsBack") do pvd2 

    for t in 0:dt:T

      function γ_cut(x)
        cut_length = t*cut_speed
        cut_end = -30.5 - cut_length
        if (x[3] > cut_end) 
        # if (x[3] > -37.5) && (x[1] < cut_end)
          return 0.0
        else
          return 1.0
        end
      end
      
      # Jump and mean operators for this formulation
      jump_u(u1,u2) = u1 - u2
      mean_t(u1,u2) = κ1*(σ1∘ε(u1)) + κ2*(σ2∘ε(u2))

      # Weak form
      # TODO: h scaling for ghost penaly is not correct yet
      a((u1,u2),(v1,v2)) =
        ∫( ε(v1) ⊙ (σ1∘ε(u1)) ) * dΩ1 +
        ∫( ε(v2) ⊙ (σ2∘ε(u2)) ) * dΩ2 + 
        ∫( γ_cut* (β_cut*jump_u(v1,v2)⋅jump_u(u1,u2)
          - n_Γ_cut⋅mean_t(u1,u2)⋅jump_u(v1,v2)
          - n_Γ_cut⋅mean_t(v1,v2)⋅jump_u(u1,u2) ) 
          )* dΓ_cut +
        #   ∫( β_D/h1*u1⋅v1
        ∫( β_D*u1⋅v1
         - n_Γ_D⋅(σ1∘ε(u1))⋅v1
         - n_Γ_D⋅(σ1∘ε(v1))⋅u1) * dΓ_D +
        # ∫( jump(u1)⋅jump(v1) ) * dΓg1 
        # ∫( β1*jump(n_Γg1⋅∇(u1))⋅jump(n_Γg1⋅∇(v1)) ) * dΓg1  +
        # ∫( β2*jump(n_Γg2⋅∇(u2))⋅jump(n_Γg2⋅∇(v2)) ) * dΓg2 
        # ∫( (β1*h1)*jump(n_Γg1⋅∇(u1))⋅jump(n_Γg1⋅∇(v1)) ) * dΓg1 
        # +
        # ∫ ( (β2*h2)*jump(n_Γg2⋅∇(u2))⋅jump(n_Γg2⋅∇(v2)))*dΓg2

      l((v1,v2)) = ∫(g ⊙ v1) * dΩ1 + ∫(g ⊙ v2) * dΩ2 

      # FE problem
      op = AffineFEOperator(a,l,U,V)
      uh1, uh2 = solve(op)
      uh = (uh1,uh2)

      pvd1[t] = createvtk(Ω1, "LinearElasticityDynamicsFront$t"*".vtu",  cellfields=["uh"=>uh1, "sigma"=>σ1∘ε(uh1)])
      pvd2[t] = createvtk(Ω2, "LinearElasticityDynamicsBack$t"*".vtu", cellfields=["uh"=>uh2, "sigma"=>σ2∘ε(uh2)])
    end
  end
end