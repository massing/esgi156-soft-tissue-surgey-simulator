## Import necessary modules and define Lame parameters
using Gridap
using GridapEmbedded
using LinearAlgebra: tr

function lame_parameters(E, ν)
    λ = (E * ν) / ((1 + ν) * (1 - 2 * ν))
    μ = E / (2 * (1 + ν))
    return (λ, ν)
end

## Define material parameters
# Material parameters 1
E1 = 59.0 
ν1 = 0.45
λ1, μ1 = lame_parameters(E1,ν1)
σ1(ε) = λ1*tr(ε)*one(ε) + 2*μ1*ε

# Material parameters 2
E2 = 59.0 
ν2 = 0.45
λ2, μ2 = lame_parameters(E2,ν2)
σ2(ε) = λ2*tr(ε)*one(ε) + 2*μ2*ε

# Dirichlet values
u0 = VectorValue(0,0,0)
u1 = VectorValue(0.2,0.0,0.0)

# Load vector
scale=1
g =scale*VectorValue(0, 0, -1.0925e-4)

## Define model for the background mesh
L = VectorValue(20.0, 20.0, 2)
n = 3
partition = (L[1]/L[3]*n, L[2]/L[3]*n, 3*n)
pmin = Point(0.,0.,0.)
pmax = pmin + L
bgmodel = CartesianDiscreteModel(pmin,pmax,partition)
Ω_bg = Triangulation(bgmodel)

# Identify Dirichlet boundaries
labeling = get_face_labeling(bgmodel)
add_tag_from_tags!(labeling,"support0", [1,3,5,7,13,15,17,19,25])
add_tag_from_tags!(labeling,"support1", [2,4,6,8,14,16,18,20,26])

writevtk(bgmodel, "bgmodel")

##  Define interface geometry
midpoint = 0.51234*L
plane_normal = VectorValue(1,0,0)
geo1 = plane(x0=midpoint, v=plane_normal, name="left")
geo2 = !(geo1, name="right")

## Discretization and integration meshes

cutgeo = cut(bgmodel,union(geo1,geo2))

# Setup interpolation mesh
Ω1_act = Triangulation(cutgeo, ACTIVE, "left")
Ω2_act = Triangulation(cutgeo, ACTIVE, "right")

# Setup integration meshes
Ω1 = Triangulation(cutgeo,PHYSICAL,"left")
Ω2 = Triangulation(cutgeo,PHYSICAL,"right")
Γ = EmbeddedBoundary(cutgeo,"left","right")

writevtk(Ω1_act, "Omega_1_act")
writevtk(Ω2_act, "Omega_2_act")
writevtk(Ω1, "Omega_1")
writevtk(Ω2, "Omega_2")
writevtk(Γ, "Gamma")

# Setup normal vectors
n_Γ = get_normal_vector(Γ)

# Setup Lebesgue measures
order = 1
degree = 2*order
dΩ1 = Measure(Ω1,degree)
dΩ2 = Measure(Ω2,degree)
dΓ = Measure(Γ,degree)


## Setup FESpace
V1 = TestFESpace(Ω1_act,
                 ReferenceFE(lagrangian,VectorValue{3,Float64},order),
                 conformity=:H1,
                 dirichlet_tags=["support0"])
V2 = TestFESpace(Ω2_act,
                 ReferenceFE(lagrangian,VectorValue{3,Float64},order),
                 conformity=:H1)
                #  ,
                #  dirichlet_tags=["support1"])

U1 = TrialFESpace(V1,[u0])
U2 = TrialFESpace(V2)
# U2 = TrialFESpace(V2, [u1])

V = MultiFieldFESpace([V1,V2])
U = MultiFieldFESpace([U1,U2])

## Set up weak variation formulation

# Setup stabilization parameters
meas_K1 = get_cell_measure(Ω1, Ω_bg)
meas_K2 = get_cell_measure(Ω2, Ω_bg)
meas_KΓ = get_cell_measure(Γ, Ω_bg)

γ_hat = 2
κ1 = CellField( (E2*meas_K1) ./ (E2*meas_K1 .+ E1*meas_K2), Ω_bg)
κ2 = CellField( (E1*meas_K2) ./ (E2*meas_K1 .+ E1*meas_K2), Ω_bg)
β  = CellField( (γ_hat*meas_KΓ) ./ ( meas_K1/E1 .+ meas_K2/E2 ), Ω_bg)

# Describe where we switch between cut and non-cut interface boundary condition
T = 10
dt = 0.5
blade_length = 0.6
cut_speed = 20.0/10.0
cut_depth = 1

## Run time loop 
createpvd("LinearElasticityDynamicsLeft") do pvd1
  createpvd("LinearElasticityDynamicsRight") do pvd2 

    for t in 0:dt:T

      function γ_cut(x)
        cut_length = blade_length + t*cut_speed
        if x[3] > 2.0 - cut_depth && x[2] <  cut_length
          return 0.0
        else
          return 1.0
        end
      end
      
      # γ_cut(x) = γ_cut(x, )

      # Jump and mean operators for this formulation
      jump_u(u1,u2) = u1 - u2
      mean_t(u1,u2) = κ1*(σ1∘ε(u1)) + κ2*(σ2∘ε(u2))

      # Weak form
      a((u1,u2),(v1,v2)) =
        ∫( ε(v1) ⊙ (σ1∘ε(u1)) ) * dΩ1 +
        ∫( ε(v2) ⊙ (σ2∘ε(u2)) ) * dΩ2 + 
        ∫( γ_cut* (β*jump_u(v1,v2)⋅jump_u(u1,u2)
          - n_Γ⋅mean_t(u1,u2)⋅jump_u(v1,v2)
          - n_Γ⋅mean_t(v1,v2)⋅jump_u(u1,u2) ) 
          )* dΓ

      l((v1,v2)) = ∫(g ⊙ v1) * dΩ1 + ∫(g ⊙ v2) * dΩ2 

      # FE problem
      op = AffineFEOperator(a,l,U,V)
      uh1, uh2 = solve(op)
      uh = (uh1,uh2)

      pvd1[t] = createvtk(Ω1, "LinearElasticityDynamicsLeft_$t"*".vtu",  cellfields=["uh"=>uh1, "sigma"=>σ1∘ε(uh1)])
      pvd2[t] = createvtk(Ω2, "LinearElasticityDynamicsRight_$t"*".vtu", cellfields=["uh"=>uh2, "sigma"=>σ2∘ε(uh2)])
    end
  end
    # pvd[t] =  createvtk(Ω1, "LinearElasticityDynamics_$t"*".vtu", cellfields=["uh"=>uh1, "sigma"=>σ1∘ε(uh1)])
  end