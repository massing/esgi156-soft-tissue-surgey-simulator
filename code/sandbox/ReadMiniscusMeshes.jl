##
using Gridap
using GridapEmbedded
using GridapGmsh

meniscus_model = GmshDiscreteModel("meshes/MEDIALMENISCUS.gmsh2")
# meniscus_labeling = get_face_labeling(meniscus_model)
# add_tag_from_tags!(labeling,"support0", [1,3,5,7,13,15,17,19,25])
# add_tag_from_tags!(labeling,"support1", [2,4,6,8,14,16,18,20,26])
writevtk(meniscus_model, "meniscus_ref0")


#  Define interface geometry
midpoint = Point(120, 50, 0)
plane_normal = VectorValue(-1,0,0)
geo1 = plane(x0=midpoint, v=plane_normal, name="right")
geo2 = !(geo1, name="left")

# Discretization and integration meshes

cutgeo = cut(meniscus_model,union(geo1,geo2))

# Setup interpolation mesh
Ω1_act = Triangulation(cutgeo, ACTIVE, "left")
Ω2_act = Triangulation(cutgeo, ACTIVE, "right")

# Setup integration meshes
Ω1 = Triangulation(cutgeo,PHYSICAL,"left")
Ω2 = Triangulation(cutgeo,PHYSICAL,"right")
Γ = EmbeddedBoundary(cutgeo,"left","right")

writevtk(Ω1_act, "men_1_act")
writevtk(Ω2_act, "men_2_act")
writevtk(Ω1, "men_1")
writevtk(Ω2, "men_2")
writevtk(Γ, "men_gamma")